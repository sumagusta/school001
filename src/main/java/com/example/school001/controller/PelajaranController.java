package com.example.school001.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PelajaranController {
	
	@RequestMapping("/home")
	public String coba() {
		return "/pelajaran/home";
	}
	
	@RequestMapping("/mtk")
	public String mtk() {
		return "/pelajaran/mtk";
	}
}

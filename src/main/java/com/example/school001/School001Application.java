package com.example.school001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class School001Application {
	
	public static void main(String[] args) {
		SpringApplication.run(School001Application.class, args);
	}
	
	@RequestMapping("/rumah")
	public String home() {
		return "/index";
	}

}
